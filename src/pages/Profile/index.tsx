import * as React from "react";
import { View, Text, ScrollView, Image, Dimensions } from "react-native";
import { Button } from 'react-native-elements';
import styled from 'styled-components';
import data from '../../../application/api';
import MenuButton from '../../assets/images/icons/menu_button.svg';
import Public from '../../assets/images/icons/public.svg';
import Marc from '../../assets/images/icons/marc.svg';
import Menu from '../../components/Menu';
import { Tab, Tabs } from 'native-base';

const Box = styled(ScrollView)`
    flex: 1;
`;

const Header = styled(View)`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 5px 10px;
    height: 50px;
    background: #FAFAFA;
`;

const Info = styled(Header)`
    height: auto;
`;

const ThumbProfile = styled(Image)`
    width: 92px;
    height: 92px;
    border-radius: 50px;
`;

const PersonalInfo = styled(View)`
    
`;

const PublicationsNumber = styled(View)`
    align-items: center;
    padding-bottom: 24px;
`;
const FollowersNumber = styled(PublicationsNumber)``;
const FollowingNumber = styled(FollowersNumber)``;

const Number = styled(Text)`
    font-weight: bold;
    font-size: 18px;
`;

const Name = styled(Text)`
    font-weight: bold;
    padding-top: 10px;
    padding-bottom: 0;
`;

const BoxPublications = styled(View)`
    flex-direction: row;
    flex-wrap: wrap;
`;

const Bio = styled(Text)`
    padding: 0 10px;
    font-size: 14px;
`;

const ButtonEditProfile = styled(View)`
    padding: 20px 10px;
    margin-bottom: 10px;
`;

const Publication = styled(View)`
    padding: 2px;
`;

const TabHeader: any = styled(View)`
    flex-direction: row;
    justify-content: space-around;
    border-style: solid;
    border-top-width: 1px;
    border-top-color: #DBDBDB;
`;

const HeaderIcon = styled(View)`
    padding: 10px 0;
`;

const Profile: any = () => {
    const { name, profile, publicationsNumber, followersNumber, followingNumber, bio } = data.data;
    const [routes] = React.useState([
        { key: 'first', title: '<Text>teste2</Text>' },
        { key: 'second', title: 'teste' },
    ]);

    const [index, setIndex] = React.useState(0);

    const win = Dimensions?.get('window');
    const ratio = win.width / 541;

    return (
        <>
            <Header>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{name?.split(' ')[0]}</Text>
                <MenuButton />
            </Header>
            <Box>
                <Info>
                    <PersonalInfo>
                        <ThumbProfile source={profile} />
                        <Name>{name}</Name>

                    </PersonalInfo>

                    <PublicationsNumber>
                        <Number>{publicationsNumber}</Number>
                        <Text>Publicações</Text>
                    </PublicationsNumber>
                    <FollowersNumber>
                        <Number>{followersNumber}</Number>
                        <Text>Seguidores</Text>
                    </FollowersNumber>
                    <FollowingNumber>
                        <Number>{followingNumber}</Number>
                        <Text>Seguindo</Text>
                    </FollowingNumber>
                </Info>
                <Bio>{bio}</Bio>
                <ButtonEditProfile>
                    <Button titleStyle={{ color: '#262626', fontSize: 14, padding: 0 }} title={'Editar perfil'} buttonStyle={{ backgroundColor: "#FFFFFF", borderStyle: 'solid', borderWidth: 1, borderColor: '#DBDBDB', height: 30 }} />
                </ButtonEditProfile>
                <TabHeader>
                    <HeaderIcon>
                        <Public width={25} height={25} />
                    </HeaderIcon>
                    <HeaderIcon>
                        <Marc width={27} height={27} />
                    </HeaderIcon>
                </TabHeader>
                <BoxPublications>
                    {data?.data?.publications.map((item, index) => (
                        <Publication key={index}>
                            <Image style={{ width: (win?.width / 3) - 4, height: ratio * 170 }} source={item?.foto} />
                        </Publication>
                    ))}
                </BoxPublications>
            </Box>
            <Menu />
        </>
    );
}

export default Profile;
