import * as React from "react";
import { View, Text, ScrollView, Image, Dimensions } from "react-native";
import styled from 'styled-components';
import data from '../../../application/api';
import { LinearGradient } from 'expo-linear-gradient';

import Camera from '../../assets/images/icons/camera.svg';
import Instagram from '../../assets/images/icons/instagram.svg';
import Message from '../../assets/images/icons/message.svg';
import Like from '../../assets/images/icons/like.svg';
import Commnent from '../../assets/images/icons/comment.svg';
import Promove from '../../assets/images/icons/promove.svg';
import MessageAction from '../../assets/images/icons/message_action.svg';
import Menu from '../../components/Menu';

const Box = styled(ScrollView)`
    flex: 1;
`;

const Stories = styled(ScrollView)`
    padding: 6px 0;
    border: 1px solid #EFEFEF;
    border-top-width: 0;
    border-left-width: 0;
    border-right-width: 0;
`;

const PostUserName = styled(Text)`
    font-weight: bold;
`;

const Posts = styled(View)`
    margin-bottom: 10px;
`;


const Story = styled(View)`
    margin-left: 15px;
`;

const Post = styled(View)`
`;

const PostContent = styled(View)`
`;

const PostActions = styled(View)`
    flex-direction: row;
    justify-content: space-between;
    padding: 10px 15px 0;
`;

const ContentText = styled(Text)``;

const ActionsLeft = styled(View)`
    flex-direction: row;
`;

const DescriptionBox = styled(View)`
    flex-direction: row;
`;

const ActionsRight = styled(View)``;

const PostUser = styled(View)`
    flex-direction: row;
    align-items: flex-end;
    padding: 8px 16px;
`;

const PostDescription = styled(Text)`
    padding: 0px 15px 10px;
`;

const StoryRadius = styled(View)`
    padding: 1px;
    border-radius: 70px;
    background: #FAFAFA;
`;

const Thumb = styled(Image)`
    width: 60px;
    height: 60px;
    border-radius: 70px;
`;

const PostThumb = styled(Thumb)`
    width: 30px;
    height: 30px;
    border-radius: 30px;
`;

const PostImage = styled(Image)`
`;

const Header = styled(View)`
    flex-direction: row;
    justify-content: space-between;
    padding: 5px 10px;
    height: 50px;
    elevation:3;
    background: #FAFAFA;
`;

const Home: any = () => {
    const win = Dimensions?.get('window');
    const ratio = win.width / 541;

    return (
        <>
            <Header>
                <Camera width={23} height={23} style={{ paddingTop: 40 }} />
                <Instagram style={{ alignSelf: 'center' }} />
                <Message width={25} height={25} style={{ paddingTop: 40 }} />
            </Header>
            <Box>
                <Stories
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ paddingHorizontal: 1 }}>
                    {data?.data?.stories?.map((item, index) => (
                        <Story key={index}>
                            <LinearGradient key={index} colors={["#F5A64E", "#D23061", "#B80C82"]}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={{ borderRadius: 70, width: 70, height: 70, padding: 4 }}>
                                <StoryRadius>
                                    <Thumb source={item.thumb} />
                                </StoryRadius>
                            </LinearGradient>

                            {index === 0 ? (
                                <Text style={{ color: "#9E9E9E" }}>Seu story</Text>
                            ) : (
                                    <Text style={{ color: "#262626" }}>{item.user}</Text>
                                )}
                        </Story>
                    ))}
                </Stories>
                <Posts>
                    {data?.data?.posts?.map((item, index) => (
                        <Post key={index}>
                            <PostUser>
                                <PostThumb source={item.thumb} />
                                <PostUserName style={{ marginLeft: 10, alignSelf: 'center' }}>{item.user}</PostUserName>
                            </PostUser>
                            <PostContent>
                                <PostImage style={{ width: win.width, height: 700 * ratio }} source={item?.photo_pub} />
                                <PostActions>
                                    <ActionsLeft>
                                        <Like width={23} height={23} />
                                        <Commnent width={23} height={23} style={{ marginLeft: 8 }} />
                                        <MessageAction width={40} height={40} style={{ marginLeft: 0, marginTop: -10 }} />
                                    </ActionsLeft>
                                    <ActionsRight>
                                        <Promove width={20} height={20} />
                                    </ActionsRight>
                                </PostActions>
                                {item.text_pub && (
                                    <DescriptionBox>
                                        <PostDescription><PostUserName>{item?.user}</PostUserName> {`${item?.text_pub}`}</PostDescription>
                                    </DescriptionBox>
                                )}
                            </PostContent>
                        </Post>
                    ))}
                </Posts>
            </Box>
            <Menu/>
        </>
    );
}

export default Home;
