import * as React from "react";
import { View, Text } from "react-native";
import Logo from '../../assets/images/icons/logo.svg';
import From from '../../assets/images/icons/from.svg';
import Facebook from '../../assets/images/icons/facebook.svg';

import styled from 'styled-components';

const Box = styled(View)`
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
`;
const BoxLogo = styled(View)`
  flex: 2;
  justify-content: flex-end;
`;

const FromBox = styled(View)`
  text-align: center;
  align-self: center;
  justify-content: center;
  flex-direction: column;
`;

const BoxDescriptions = styled(View)`
  flex: 1;
  justify-content: flex-end;
  padding-bottom: 55px;
`;



const Loading: any = () => {
  return (
    <>
      <Box>
        <BoxLogo>
          <Logo width={100} style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }} />
        </BoxLogo>
        <BoxDescriptions>
          <From width={30} style={{alignSelf: 'center', marginBottom: 5}} />
          <Facebook />
        </BoxDescriptions>
      </Box>

    </>
  );
}

export default Loading;
