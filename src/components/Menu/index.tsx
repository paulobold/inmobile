import * as React from "react";
import { View, Image } from "react-native";
import styled from 'styled-components';
import data from '../../../application/api';
import HomeIcon from '../../assets/images/icons/home.svg';
import Notification from '../../assets/images/icons/notications.svg';
import Publicate from '../../assets/images/icons/publicate.svg';
import Search from '../../assets/images/icons/search.svg';

const Thumb = styled(Image)`
    width: 60px;
    height: 60px;
    border-radius: 70px;
`;

const PostThumb = styled(Thumb)`
    width: 30px;
    height: 30px;
    border-radius: 30px;
`;

const Box = styled(View)`
    height: 50px;
    padding: 0 15px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    elevation: 6;
`;

const Menu = () => {
    return (
        <Box>
            <HomeIcon width={23} height={23}/>
            <Search width={23} height={23} />
            <Publicate width={23} height={23}/>
            <Notification width={23} height={23}/>
            <PostThumb source={data?.data?.profile} />
        </Box>
    );
}

export default Menu;
