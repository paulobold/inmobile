import * as React from "react";

const api = {
    data: {
        name: 'Thaysa Gomes',
        profile: require('../src/assets/images/stories/elipse1.jpg'),
        bio: 'Sempre que vou lavar o cabelo, eu danço comigo mesma ao som de Beach Boys.',
        publicationsNumber: 136,
        followersNumber: 375,
        followingNumber: 589,
        publications: [
            {
                foto: require('../src/assets/images/publications/public1.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public2.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public3.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public4.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public5.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public6.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public7.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public8.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public9.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public10.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public11.jpg'),
                description: 'teste'
            },
            {
                foto: require('../src/assets/images/publications/public12.jpg'),
                description: 'teste'
            }
        ],
        stories: [
            {
                user: 'Thaysa Gomes',
                thumb: require('../src/assets/images/stories/elipse1.jpg'),
                photos: [
                    require('../src/assets/images/stories/elipse1.jpg'),
                    require('../src/assets/images/stories/elipse1.jpg')
                ]
            },
            {
                user: 'uxunicornio',
                thumb: require('../src/assets/images/stories/elipse2.jpg'),
                photos: [
                ]
            },
            {
                user: 'nina_talks',
                thumb: require('../src/assets/images/stories/elipse3.jpg'),
                photos: [
                ]
            },
            {
                user: 'ladiesthatuxr...',
                thumb: require('../src/assets/images/stories/elipse4.jpg'),
                photos: [
                ]
            },
            {
                user: 'ayama.design',
                thumb: require('../src/assets/images/stories/elipse5.jpg'),
                photos: [
                ]
            },
        ],
        posts: [
            {   
                
                user: 'ayama.design',
                thumb: require('../src/assets/images/stories/elipse5.jpg'),
                photo_pub: require('../src/assets/images/post/post1.jpg'),
                text_pub: '⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣Já passou da hora de pensarmos além de nós mesmos',
                likes: 0,
                comments: [
                    {
                        text: '',
                        replies: []
                    }
                ]
            },
            {   
                
                user: 'uxunicornio',
                thumb: require('../src/assets/images/stories/elipse2.jpg'),
                photo_pub: require('../src/assets/images/post/post2.jpg'),
                text_pub: '⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣⁣Ligue o som para os dois últimos slides do Saimon!...',
                likes: 0,
                comments: [
                    {
                        text: '',
                        replies: []
                    }
                ]
            },
            {   
                
                user: 'nina_talks',
                thumb: require('../src/assets/images/stories/elipse3.jpg'),
                photo_pub: require('../src/assets/images/post/post3.jpg'),
                text_pub: 'Todos nós ficamos inspirados e empolgados quando vemos alguém que refle tudo aquilo que...',
                likes: 0,
                comments: [
                    {
                        text: '',
                        replies: []
                    }
                ]
            },
            {   
                
                user: 'ladiesthatuxrec',
                thumb: require('../src/assets/images/stories/elipse4.jpg'),
                photo_pub: require('../src/assets/images/post/post4.jpg'),
                text_pub: 'Cada cidade conta com sua própria dinâmica, agenda e formato de eventos mas meetups',
                likes: 0,
                comments: [
                    {
                        text: '',
                        replies: []
                    }
                ]
            }
        ]
    }
}

export default api;