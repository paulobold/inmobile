import * as React from "react";
import { View, Text, StatusBar } from "react-native";
import Loading from './src/pages/Loading';
import Profile from "./src/pages/Profile";

export default function App() {
  return (
    <View style={{ backgroundColor: '#FAFAFA', flex: 1 }}>
      <StatusBar />
      {/* <Home /> */}
      <Profile />
    </View>
  );
}
